<?php
	require_once('filebrowser.php');

	/*
	 * Add your filebrowser definition code here
	 */
	$fileBrowser = new FileBrowser(__DIR__, null, ['php']);
	$files = $fileBrowser->Get();
?>
<!doctype html>
<html lang="en">
 <head>
  <title>File browser</title>
 </head>
 <body>
  <!-- Output file list HTML here -->
  <ul>
<?php if (count($files)): ?>
<?php  foreach ($files as $file): ?>
   <li><?=$file?></li>
<?php  endforeach; ?>
<?php else: ?>
   <li>Empty directory</li>
<?php endif; ?>
  </ul>
 </body>
</html>
