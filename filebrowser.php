<?php
	require_once('interface.php');

	class FileBrowser implements __FileBrowser {
		private $rootPath = null;
		private $currentPath = null;
		private $extensionfilter = [];
		private $lastError = null;
		private $handle = null;

		function __construct($rootPath, $currentPath = null, array $extensionFilter = array()) {
			$this->SetRootPath($rootPath);
			$this->SetCurrentPath($currentPath);
			$this->SetExtensionFilter($extensionFilter);
		}

		function SetRootPath($rootPath) {
			if (!file_exists($rootPath)) {
				$this->lastError = 'Non-existing directory';
				return false;
			} elseif (!is_dir($rootPath)) {
				$this->lastError = 'Non-directory';
				return false;
			} elseif (!is_readable($rootPath)) {
				$this->lastError = 'Unreadable directory';
				return false;
			}
			if (strrpos($rootPath, DIRECTORY_SEPARATOR) < strlen($rootPath) - 1) {
				$rootPath .= DIRECTORY_SEPARATOR;
			}
			$this->rootPath = $rootPath;
			return true;
		}

		function SetCurrentPath($currentPath) {
			if (in_array($currentPath, [null, '.', DIRECTORY_SEPARATOR])) {
				$currentPath = '';
			}
			if (strpos($currentPath, DIRECTORY_SEPARATOR) === 0) {
				$currentPath = substr($currentPath, 1);
			}
			$path = $this->rootPath . $currentPath;
			if (strpos($path, $this->rootPath) !== 0) {
				$this->lastError = 'Invalid directory';
				return false;
			} elseif (!file_exists($path)) {
				$this->lastError = 'Non-existing directory';
				return false;
			} elseif (!is_dir($path)) {
				$this->lastError = 'Non-directory';
				return false;
			} elseif (!is_readable($path)) {
				$this->lastError = 'Non-readable directory';
				return false;
			}
			$this->currentPath = $currentPath;
			$this->handle = opendir($path);
			return true;
		}

		function SetExtensionFilter(array $extensionFilter) {
			$this->extensionFilter = $extensionFilter;
		}

		function Get() {
			if (!$this->handle) {
				return [];
			}
			$files = [];
			while (($file = readdir($this->handle)) !== false) {
				if ($file == '.' || $file == '..') {
					// skip these pseudo files
					continue;
				}
				$path = $this->rootPath . $this->currentPath . $file;
				if (is_dir($path)) {
					// skip directory ?
					continue;
				}
				if ($this->extensionFilter) {
					$finfo = pathinfo($path);
					if (!in_array($finfo['extension'], $this->extensionFilter)) {
						// filter in
						continue;
					}
				}
				$files[] = $file;
			}
			return $files;
		}

		function GetLastError() {
			return $this->lastError;
		}
	}

